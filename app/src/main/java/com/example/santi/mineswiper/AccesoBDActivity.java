package com.example.santi.mineswiper;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import com.example.santi.mineswiper.QueryFrag.GameListener;


public class AccesoBDActivity extends FragmentActivity implements GameListener{

    /**Mètodes de creació de fragment i tractament, cas tablet incorporat**/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceso_bd);
        QueryFrag queryFrag = (QueryFrag)getSupportFragmentManager().findFragmentById(R.id.queryFrag);
        queryFrag.setGameListener(this);
    }

    @Override
    public void onGameSelected(LogInfo2 info){
        boolean hayDetalle = (getSupportFragmentManager().findFragmentById(R.id.detailFrag)!=null);
        if (hayDetalle){
            ((RegistroFrag)getSupportFragmentManager().findFragmentById(R.id.detailFrag)).showDetail(info);
        }
        else {
            Intent i = new Intent(this,DetalleRegActivity.class);
            i.putExtra("dades",info);
            startActivity(i);
        }
    }
}
