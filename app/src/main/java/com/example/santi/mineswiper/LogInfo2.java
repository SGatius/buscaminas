package com.example.santi.mineswiper;

public class LogInfo2 extends LogInfo {
    private String coordenates;

    public LogInfo2(String userName, String date, int nCasillasEscogidas, int percentMines, int mines, String msg, String coordenates) {
        super(userName, date, nCasillasEscogidas, percentMines, mines, msg);
        this.coordenates = coordenates;
    }

    public String getCoordenates() {
        return coordenates;
    }

    public void setCoordenates(String coordenates) {
        this.coordenates = coordenates;
    }

    @Override
    public String toString() {
        return "LogInfo2{" +
                "coordenates='" + coordenates + '\'' +
                "} " + super.toString();
    }
}
