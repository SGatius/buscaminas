package com.example.santi.mineswiper;

import android.app.ActionBar;
import android.app.Activity;
import android.preference.PreferenceFragment;
import android.os.Bundle;

public class SharedActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SharedFrg()).commit();

    }

    public static class SharedFrg extends PreferenceFragment{

        @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.opciones);
        }

    }
}
