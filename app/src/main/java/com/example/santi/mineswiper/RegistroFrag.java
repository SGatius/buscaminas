package com.example.santi.mineswiper;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegistroFrag extends Fragment {


    public RegistroFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registro, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state){

        super.onActivityCreated(state);
        Button btnBack = (Button)getView().findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
    }


        public void showDetail(LogInfo2 info){
        TextView user = (TextView)getView().findViewById(R.id.userName);
        TextView data = (TextView)getView().findViewById(R.id.data);
        TextView ncasillas = (TextView)getView().findViewById(R.id.ncasillas);
        TextView percentatge = (TextView)getView().findViewById(R.id.percentageMines);
        TextView nmines = (TextView)getView().findViewById(R.id.nMines);
        TextView status = (TextView)getView().findViewById(R.id.status);
        TextView coordenates = (TextView)getView().findViewById(R.id.coordenates);
        user.setText(info.getUserName());
        data.setText(info.getDate());
        ncasillas.setText(String.valueOf(info.getnCasillasEscogidas()));
        percentatge.setText(String.valueOf(info.getPercentMines()));
        nmines.setText(String.valueOf(info.getMines()));
        status.setText(info.getMsg());
        coordenates.setText(info.getCoordenates());
        }
}
