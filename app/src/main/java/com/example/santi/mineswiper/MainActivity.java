package com.example.santi.mineswiper;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    /** Variables privades **/

    private SharedPreferences preferences;
    private String username, sizeGrill;
    private int sizegrill;

    /**Mètode onCreate**/
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnGame = (Button)findViewById(R.id.buttonStart);
        try {
            btnGame.setOnClickListener(this);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    /**Mètode onClick receptor del botó per començar a jugar**/
    @Override
    public void onClick(View v) {
        preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        this.username = preferences.getString("editTextAlias","Player");
        this.sizeGrill = preferences.getString("editTextSizeGrill","25");

        if (username.trim().isEmpty()){
            showToast(R.string.msg1);
        }
        else if (sizeGrill.trim().isEmpty()){
            showToast(R.string.msg3);
        }
        else {
            this.sizegrill = Integer.parseInt(sizeGrill);
            if (sizegrill == 0 || sizegrill == 1) {
                showToast(R.string.msg2);
            } else {
                Intent intent = new Intent(MainActivity.this, GameActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("USERNAME", username);
                bundle.putInt("SIZEGRILL", sizegrill);
                bundle.putBoolean("CHECKCONTROL", preferences.getBoolean("checkBoxTime", false));
                bundle.putInt("MINES", Integer.parseInt(preferences.getString("porcentajeMinas", "25")));
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        }
    }
    /**Mètode auxiliar que mostra un Toast del feedback**/
    private void showToast(int id){
        Toast.makeText(this, id,Toast.LENGTH_SHORT).show();
    }
    /**Part de creació de menú i tractament**/
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.configuracio:
                startActivity(new Intent(this,SharedActivity.class));
        }
        return true;
    }
    /**Fi part de creació de menú i tractament**/

    /**Mètodes de tractament onClick en xml**/
    public void goHelp(View v){
        startActivity(new Intent(this,HelpActivity.class));
        finish();
    }
    public void goStatistics(View v){
        startActivity(new Intent(this, AccesoBDActivity.class));
        finish();
    }
    public void goExit(View v){
        finish();
    }
}
