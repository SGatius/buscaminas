package com.example.santi.mineswiper;

import java.io.Serializable;


public class LogInfo implements Serializable{

    private String userName;
    private String date;
    private int nCasillasEscogidas;
    private int percentMines;
    private int mines;
    private String msg;

    public LogInfo(String userName,String date, int nCasillasEscogidas, int percentMines, int mines, String msg){
        super();
        this.userName = userName;
        this.date = date;
        this.nCasillasEscogidas = nCasillasEscogidas;
        this.percentMines = percentMines;
        this.mines = mines;
        this.msg = msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserName() {
        return userName;
    }

    public int getnCasillasEscogidas() {
        return nCasillasEscogidas;
    }

    public int getPercentMines() {
        return percentMines;
    }

    public int getMines() {
        return mines;
    }

    public String getMsg() {
        return msg;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setnCasillasEscogidas(int nCasillasEscogidas) {
        this.nCasillasEscogidas = nCasillasEscogidas;
    }

    public void setPercentMines(int percentMines) {
        this.percentMines = percentMines;
    }

    public void setMines(int mines) {
        this.mines = mines;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString(){
        return "Resultados partida:"
                + "\n\tNom: " + userName
                + "\n\tCaselles: " + String.valueOf(nCasillasEscogidas)
                + "\n\tPercentatge Mines: " + String.valueOf(percentMines)
                + "\n\tMines: " + String.valueOf(mines)
                + "\n\tEstat partida " + msg;
    }
}
