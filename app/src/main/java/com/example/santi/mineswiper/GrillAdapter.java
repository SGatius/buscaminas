package com.example.santi.mineswiper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class GrillAdapter extends BaseAdapter{
    private int sizeGrill, percentMines, minesToBomb, numFlags;
    private String userName;
    private GameListener listener;
    Context context;
    List<Element>table;

    public GrillAdapter(Context context, List<Element>table, int sizeGrill, String userName,
                        int percentMines, int minesToBomb) {
        this.context = context;
        this.table = table;
        this.sizeGrill = sizeGrill;
        this.userName = userName;
        this.percentMines = percentMines;
        this.minesToBomb = minesToBomb;
        this.numFlags = 0;
    }

    @Override
    public int getCount() {
        return sizeGrill*sizeGrill;
    }

    @Override
    public Element getItem(int position) {
        return table.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Button btn;
        if (convertView == null){
            btn = new Button(context);
            btn.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.WRAP_CONTENT,GridView.LayoutParams.WRAP_CONTENT));
            btn.setPadding(8,8,8,8);
        }
        else{
            btn = (Button)convertView;
            btn.setPadding(8,8,8,8);
        }
        btn.setId(position);
        btn.setOnClickListener(new MyOnClickListener(position));
        btn.setOnLongClickListener(new MyOnLongClickListener(position));
        return btn;
    }

    public class MyOnLongClickListener extends Activity implements View.OnLongClickListener{

        private final int position;
        public MyOnLongClickListener(int position){this.position = position;}

        @Override
        public boolean onLongClick(View v) {
            Button btn = (Button) v.findViewById(v.getId());
            Element e = table.get(position);
            String coord = "("+getX(e.getPosition())+"," + getY(e.getPosition())+")";
            SimpleDateFormat data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            LogInfo2 info = new LogInfo2(userName,data.format(new Date()),sizeGrill,percentMines,minesToBomb,"Pendent",coord);
            if (listener!=null){
                listener.onGameSelected(info);
            }
            if (e.isCovered()){
                if (numFlags < minesToBomb) {
                    numFlags = numFlags + 1;
                    e.setCovered(false);
                    e.setQuestioned(true);
                    btn.setBackgroundResource(R.drawable.minesweeperflag);
                    table.set(position, e);
                }
                else {
                    showToast("Has de treure alguna bandera");
                }
            }
            else if (!e.isCovered() && e.isQuestioned()){
                numFlags = numFlags - 1;
                e.setCovered(true);
                e.setQuestioned(false);
                btn.setBackgroundResource(0);
                table.set(position,e);
            }
            checkGame();
            return true;
        }
    }

    private void showToast(String s) {
        Toast.makeText(context,s,Toast.LENGTH_SHORT).show();
    }

    public class MyOnClickListener extends AppCompatActivity implements View.OnClickListener {

        private final int position;
        public MyOnClickListener(int position){
            this.position=position;
        }
        @Override
        public void onClick(View view){
            Button btn = (Button) view.findViewById(view.getId());
            Element e = table.get(position);
            if(e.isCovered()){
                e.setCovered(false);
                table.set(position,e);
                String coord = "("+getX(e.getPosition())+"," + getY(e.getPosition())+")";
                SimpleDateFormat data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                LogInfo2 info = new LogInfo2(userName,data.format(new Date()),sizeGrill,percentMines,minesToBomb,"Pendent",coord);
                if (listener!=null){
                    listener.onGameSelected(info);
                }
                if(e.isMined()){
                    btn.setBackgroundResource(R.drawable.bombend);
                    gameOver("You lose", e.getPosition());
                }
                else{
                    btn.setText((CharSequence)Integer.toString(e.getNumMinesAround()));
                }
            }
            checkGame();
        }
    }
    private void checkGame(){
        if (check()){
            gameOver("YOU WIN",0);
        }
    }
    private boolean check(){
        boolean check = true;
        for(int i = 0; i < table.size(); i++){
            Element e = table.get(i);
            if (e.isCovered()){
                check = false;
            }
        }
        return check;
    }
    private void gameOver(String msg,int pos){
        end(msg, pos);
    }
    public void end(String msg, int pos){
        SimpleDateFormat data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        LogInfo logInfo = new LogInfo(userName,data.format(new Date()),getCount(),percentMines,minesToBomb,msg);
        Activity a = (Activity)context;
        Intent in = new Intent(context, LogActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("LOG",logInfo);
        bundle.putInt("xPos",getX(pos));
        bundle.putInt("yPos",getY(pos));
        in.putExtras(bundle);
        a.startActivity(in);
        a.finish();
    }

    public int getX(int pos){return pos/sizeGrill;}
    public int getY(int pos){return pos%sizeGrill;}

    public interface GameListener{
        void onGameSelected(LogInfo2 logInfo2);
    }
    public void setGameListener(GameListener listener) {
        this.listener = listener;
    }

}
