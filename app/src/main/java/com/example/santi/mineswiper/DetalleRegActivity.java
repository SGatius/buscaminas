package com.example.santi.mineswiper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DetalleRegActivity extends FragmentActivity {

    private Bundle bundle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_reg);
        bundle = getIntent().getExtras();
        LogInfo2 info = (LogInfo2)bundle.getSerializable("dades");;
        RegistroFrag detail = (RegistroFrag)getSupportFragmentManager().
                findFragmentById(R.id.detailFrag);
        Button button = (Button)findViewById(R.id.btnBack);
        button.setVisibility(View.VISIBLE);
        detail.showDetail(info);
    }
}
