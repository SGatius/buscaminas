package com.example.santi.mineswiper;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class QueryFrag extends Fragment {

    private SQLiteDatabase db;
    private ArrayList<LogInfo2> datos = new ArrayList<>();
    private ListView listView;
    private GameListener listener;
    private AdaptadorStatistics adaptadorStatistics;
    Cursor cursor;

    public QueryFrag() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_query, container, false);
    }

    @Override
    public void onActivityCreated(Bundle state){
        super.onActivityCreated(state);

        GameSQLiteHelper usdbh =
                new GameSQLiteHelper(getContext(), "DBGame", null, 1);

        db = usdbh.getReadableDatabase();
        this.cursor = db.rawQuery("SELECT * FROM Match", null);
        if (cursor.moveToFirst()){
            do {
                String name = cursor.getString(1);
                String fecha = cursor.getString(2);
                int nCasillasEscogidas = cursor.getInt(3);
                int percentMines = cursor.getInt(4);
                int mines = cursor.getInt(5);
                String msg = cursor.getString(6);
                String coordenates = cursor.getString(7);
                LogInfo2 aux = new LogInfo2(name,fecha,nCasillasEscogidas,percentMines,mines,msg,coordenates);
                datos.add(aux);
            }while (cursor.moveToNext());
        }
        cursor.close();
        Button btnMP = (Button)getView().findViewById(R.id.btnMP);
        listView = (ListView)getView().findViewById(R.id.LstGames);
        adaptadorStatistics = new AdaptadorStatistics(this,datos);
        listView.setAdapter(adaptadorStatistics);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listener != null){
                    listener.onGameSelected((LogInfo2)listView.getAdapter().getItem(position));
                }
            }
        });
        listView.setOnCreateContextMenuListener(this);
        btnMP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),MainActivity.class));
                getActivity().finish();
            }
        });
    }

    /** Creació del menú contextual, es modifica títol **/

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo menuInfo){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        String title = ((TextView)info.targetView.findViewById(R.id.alias)).getText().toString();
        contextMenu.setHeaderTitle(title);
        getActivity().getMenuInflater().inflate(R.menu.menu_list_view, contextMenu);
    }

    /** Sel·lecció d'opció corresponent**/

    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.consOne:
                showRequestAlias(info.position);
                return true;

            case R.id.sendValues:
                sendEmail(info.position);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    /** Mètodes del menú contextual **/

    private void showRequestAlias(int position){
        LogInfo2 info = (LogInfo2)listView.getAdapter().getItem(position);
        String[] alias = new String[]{info.getUserName()};
        String[] campos = new String[]{"_id","nombre","fecha","casillasEscogidas","porcentajeMinas","numeroMinas","resultadoPartida","coordenates"};
        Cursor cursorl = db.query("Match",campos,"nombre=?", alias, null, null, null);
        this.cursor.close();
        datos.removeAll(datos);
        this.cursor = cursorl;
        if (cursor.moveToFirst()){
            do {
                String name = cursor.getString(1);
                String fecha = cursor.getString(2);
                int nCasillasEscogidas = cursor.getInt(3);
                int percentMines = cursor.getInt(4);
                int mines = cursor.getInt(5);
                String msg = cursor.getString(6);
                String coord = cursor.getString(7);
                LogInfo2 aux = new LogInfo2(name,fecha,nCasillasEscogidas,percentMines,mines,msg,coord);
                datos.add(aux);
            }while (cursor.moveToNext());
        }
        adaptadorStatistics = new AdaptadorStatistics(this,datos);
        adaptadorStatistics.notifyDataSetChanged();
        listView.setAdapter(adaptadorStatistics);
    }

    public void sendEmail(final int position){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.dialogfragment,null);
        final PopupWindow popupEmail = new PopupWindow(popupView, LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT,true);
        popupEmail.showAtLocation(listView, Gravity.CENTER,10,20);
        popupEmail.setFocusable(true);
        final EditText email = (EditText)popupEmail.getContentView().findViewById(R.id.emailToSend);
        final String mail = email.getText().toString();
        Button close = (Button)popupEmail.getContentView().findViewById(R.id.btnOK);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!email.getText().toString().trim().isEmpty()){
                    popupEmail.dismiss();
                    LogInfo info = (LogInfo)listView.getAdapter().getItem(position);
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String []{mail});
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject));
                    emailIntent.putExtra(Intent.EXTRA_TEXT, info.toString());
                    startActivity(Intent.createChooser(emailIntent,  getString(R.string.sendMail)));
                }
                else {
                    showToast(R.string.introMail);
                }
            }
        });
    }

    /** Fi de mètodes del menú contextual **/

    /** Mètodes auxiliars menú contextual **/

    private void showToast(int msg){
        Toast.makeText(getContext(),msg, Toast.LENGTH_SHORT).show();
    }

    /** Fi mètodes auxiliar menú contextual **/

    /** Adaptador pel ListView de les dades **/

    class AdaptadorStatistics extends ArrayAdapter<LogInfo2>{
        Activity context;

        AdaptadorStatistics(QueryFrag queryFrag, ArrayList<LogInfo2>datos){
            super(queryFrag.getActivity(),R.layout.listitem, datos);
            this.context = queryFrag.getActivity();
        }
        public View getView(int position, View convertView, ViewGroup parent){
            LogInfo info = datos.get(position);
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listitem, null);
            TextView alias = (TextView)item.findViewById(R.id.alias);
            alias.setText(info.getUserName());
            TextView fecha = (TextView)item.findViewById(R.id.fecha);
            fecha.setText(info.getDate());
            TextView result = (TextView)item.findViewById(R.id.result);
            result.setText(info.getMsg());
            return (item);
        }
    }

    public interface GameListener{
        void onGameSelected(LogInfo2 info);
    }

    public void setGameListener(GameListener listener) {
        this.listener = listener;
    }

}
