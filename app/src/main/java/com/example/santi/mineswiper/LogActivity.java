package com.example.santi.mineswiper;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LogActivity extends AppCompatActivity implements View.OnClickListener{

    /**Declaracions de variables privades**/
    private String fecha, log;
    private SQLiteDatabase db;
    private SharedPreferences preferences;
    private String username, sizeGrill, coordenates;
    private int sizegrill;

    /**Mètode onCreate**/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        Bundle bundle = getIntent().getExtras();
        LogInfo logInfo = (LogInfo) bundle.getSerializable("LOG");
        this.coordenates = "("+bundle.getInt("xPos")+","+bundle.getInt("yPos")+")";
        log = logInfo.toString();
        fecha = logInfo.getDate();
        ((TextView) findViewById(R.id.data)).setText(fecha);
        ((TextView) findViewById(R.id.logTextView)).setText(log);

        /*Obrim la BBDD e introduim les dades*/
        GameSQLiteHelper dbgame = new GameSQLiteHelper(this, "DBGame", null, 1);
        db = dbgame.getWritableDatabase();
        ContentValues info = new ContentValues();
        info.put("nombre",logInfo.getUserName());
        info.put("fecha",fecha);
        info.put("casillasEscogidas",logInfo.getnCasillasEscogidas());
        info.put("porcentajeMinas",logInfo.getPercentMines());
        info.put("numeroMinas",logInfo.getMines());
        info.put("resultadoPartida",logInfo.getMsg());
        if (logInfo.getMsg().equals("You lose")){
            info.put("coordenates",coordenates);
        }
        else info.put("coordenates",0);
        /*Control d'errors de la inserció*/
        try {
            db.insertOrThrow("Match",null, info);
            showToast(R.string.insertCorrect);
        }catch (SQLException e){
            showToast(R.string.insertError);
        }
        /*Tanquem la BBDD*/
        db.close();
        /**Escoltadors d'events de botons e identificadors**/
        Button btnNewGame = (Button) findViewById(R.id.buttonNewGame);
        Button btnSendEmail = (Button)findViewById(R.id.buttonSendEmail);
        Button btnExit = (Button)findViewById(R.id.buttonExit);
        try {
            btnNewGame.setOnClickListener(this);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        try {
            btnSendEmail.setOnClickListener(this);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        try {
            btnExit.setOnClickListener(this);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    /**Afegim menú per configuració**/

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.configuracio:
                startActivity(new Intent(this,SharedActivity.class));
        }
        return true;
    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.buttonExit:
                finish();
                break;

            case R.id.buttonSendEmail:
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                EditText edt = (EditText)findViewById(R.id.emailText);
                String mail = edt.getText().toString();
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String []{mail});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject));
                emailIntent.putExtra(Intent.EXTRA_TEXT, log);
                startActivity(Intent.createChooser(emailIntent, getString(R.string.sendMail)));
                break;

            case R.id.buttonNewGame:
                preferences = PreferenceManager.getDefaultSharedPreferences(this);
                this.username = preferences.getString("editTextAlias","Player");
                this.sizeGrill = preferences.getString("editTextSizeGrill","25");
                if (username.trim().isEmpty()){
                    showToast(R.string.msg1);
                }
                else if (sizeGrill.trim().isEmpty()){
                    showToast(R.string.msg3);
                }
                else {
                    this.sizegrill = Integer.parseInt(sizeGrill);
                    if (sizegrill == 0 || sizegrill == 1){
                        showToast(R.string.msg2);
                    }
                    else {
                        Intent intent = new Intent(this, GameActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("USERNAME",username);
                        bundle.putInt("SIZEGRILL",sizegrill);
                        bundle.putBoolean("CHECKCONTROL",preferences.getBoolean("checkBoxTime",false));
                        bundle.putInt("MINES",Integer.parseInt(preferences.getString("porcentajeMinas","25")));
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();
                    }
                }
                break;
        }
    }
    /**Finalització del tractament del menú**/
    /**Mètode auxiliar que mostra un Toast del feedback**/
    private void showToast(int id){
        Toast.makeText(this, id,Toast.LENGTH_SHORT).show();
    }
}
