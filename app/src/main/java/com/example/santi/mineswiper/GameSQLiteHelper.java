package com.example.santi.mineswiper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class GameSQLiteHelper extends SQLiteOpenHelper {

    String sqlCreate = "CREATE TABLE Match " +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " nombre TEXT, " +
            " fecha TEXT, " +
            " casillasEscogidas INTEGER, " +
            " porcentajeMinas INTEGER, " +
            " numeroMinas INTEGER, " +
            " resultadoPartida TEXT, " +
            " coordenates TEXT)";

    public GameSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                            int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(sqlCreate);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS Match");
        db.execSQL(sqlCreate);
    }
}
